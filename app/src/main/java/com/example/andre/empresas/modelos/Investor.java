package com.example.andre.empresas.modelos;

import java.io.Serializable;

/**
 * Created by andre on 25/02/18.
 */

public class Investor implements Serializable{
    private int id, balance, portifolio_value;
    private String investor_name, city, country;
    private boolean first_access, super_angel;

    public Investor(int id, int balance, int portifolio_value, String investor_name, String city, String country, boolean first_access, boolean super_angel) {
        this.id = id;
        this.balance = balance;
        this.portifolio_value = portifolio_value;
        this.investor_name = investor_name;
        this.city = city;
        this.country = country;
        this.first_access = first_access;
        this.super_angel = super_angel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getPortifolio_value() {
        return portifolio_value;
    }

    public void setPortifolio_value(int portifolio_value) {
        this.portifolio_value = portifolio_value;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isFirst_access() {
        return first_access;
    }

    public void setFirst_access(boolean first_access) {
        this.first_access = first_access;
    }

    public boolean isSuper_angel() {
        return super_angel;
    }

    public void setSuper_angel(boolean super_angel) {
        this.super_angel = super_angel;
    }
}
