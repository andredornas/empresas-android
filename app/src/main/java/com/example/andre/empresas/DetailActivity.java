package com.example.andre.empresas;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.andre.empresas.modelos.Enterprise;
import com.example.andre.empresas.modelos.User;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public class DetailActivity extends AppCompatActivity {

    private User user;
    private Enterprise enterprise;

    private ImageView imageEnterprise;
    private TextView descriptionEnterprise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        user = (User) getIntent().getSerializableExtra("user");

        imageEnterprise = (ImageView) findViewById(R.id.image_enterprise);
        descriptionEnterprise = (TextView) findViewById(R.id.description_enterprise);

        int id = getIntent().getIntExtra("empresa", 0);
        new SearchTask(id, Glide.with(this)).execute((Void) null);
    }

    class WrappedResponse {
        private Enterprise enterprise;

        public Enterprise getEnterprise() {
            return enterprise;
        }

        public void setEnterprise(Enterprise enterprise) {
            this.enterprise = enterprise;
        }
    }

    interface SearchService {
        @Headers({
                "Content-Type: application/json"
        })
        @GET("enterprises/{id}")
        Call<WrappedResponse> search(
                @Header("access-token") String accessToken,
                @Header("client") String client,
                @Header("uid") String uid,
                @Path("id") int id
        );
    }

    public class SearchTask extends AsyncTask<Void, Void, Void> {

        private final int id;
        private final RequestManager glide;

        SearchTask(int id, RequestManager glide) {
            this.id = id; this.glide = glide;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://54.94.179.135:8090/api/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            SearchService service = retrofit.create(SearchService.class);

            Call<WrappedResponse> search = service.search(
                    user.getAccessToken(),
                    user.getClient(),
                    user.getUid(),
                    id);

            try {
                enterprise = search.execute().body().enterprise;
                System.out.println(enterprise);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            getSupportActionBar().setTitle(enterprise.getEnterprise_name());
            descriptionEnterprise.setText(enterprise.getDescription());

            if (enterprise.getPhoto() != null) {
                glide
                        .load(enterprise.getPhoto())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(imageEnterprise);
            } else {
                glide
                        .load(R.drawable.img_e_1)
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(imageEnterprise);
            }
        }
    }
}
