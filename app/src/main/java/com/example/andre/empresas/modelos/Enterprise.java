package com.example.andre.empresas.modelos;

import java.io.Serializable;

/**
 * Created by andre on 25/02/18.
 */

public class Enterprise implements Serializable{
    private int id, value, share_price;
    private String facebook, twitter, linkedin, phone;
    private boolean own_enterprise;
    private String enterprise_name, photo, description, city, country;
    private EnterpriseType enterprise_type;

    public Enterprise(int id, int value, int share_price, String facebook, String twitter, String linkedin, String phone, boolean own_enterprise, String enterprise_name, String description, String city, String country, EnterpriseType enterprise_type) {
        this.id = id;
        this.value = value;
        this.share_price = share_price;
        this.facebook = facebook;
        this.twitter = twitter;
        this.linkedin = linkedin;
        this.phone = phone;
        this.own_enterprise = own_enterprise;
        this.enterprise_name = enterprise_name;
        this.description = description;
        this.city = city;
        this.country = country;
        this.enterprise_type = enterprise_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getShare_price() {
        return share_price;
    }

    public void setShare_price(int share_price) {
        this.share_price = share_price;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isOwn_enterprise() {
        return own_enterprise;
    }

    public void setOwn_enterprise(boolean own_enterprise) {
        this.own_enterprise = own_enterprise;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public void setEnterprise_name(String enterprise_name) {
        this.enterprise_name = enterprise_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public EnterpriseType getEnterprise_type() {
        return enterprise_type;
    }

    public void setEnterprise_type(EnterpriseType enterprise_type) {
        this.enterprise_type = enterprise_type;
    }
}
