package com.example.andre.empresas.modelos;

import java.io.Serializable;

/**
 * Created by andre on 25/02/18.
 */

public class EnterpriseType implements Serializable{
    private int id;
    private String enterprise_type_name;

    public EnterpriseType(int id, String enterpriseTypeName) {
        this.id = id;
        this.enterprise_type_name = enterpriseTypeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnterprise_type_name() {
        return enterprise_type_name;
    }

    public void setEnterprise_type_name(String enterprise_type_name) {
        this.enterprise_type_name = enterprise_type_name;
    }
}
