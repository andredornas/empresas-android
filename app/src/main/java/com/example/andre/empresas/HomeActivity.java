package com.example.andre.empresas;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.andre.empresas.modelos.Enterprise;
import com.example.andre.empresas.modelos.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public class HomeActivity extends AppCompatActivity {

    private User user;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private TextView searchMessage;

    private List<Enterprise> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        user = (User) getIntent().getSerializableExtra("user");

        /*Toolbar toolbarActionBar = (Toolbar) findViewById(R.id.toolbar_action_bar);
        setSupportActionBar(toolbarActionBar);*/

        getSupportActionBar().setLogo(R.drawable.logo_home);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.logo_view);

        Toolbar parent = (Toolbar) getSupportActionBar().getCustomView().getParent();
        parent.setContentInsetsAbsolute(0,0);

        results = new ArrayList<>();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new MyAdapter(results, Glide.with(this));
        recyclerView.setAdapter(mAdapter);

        searchMessage = (TextView) findViewById(R.id.search_message);

        if (results.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            searchMessage.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            searchMessage.setVisibility(View.GONE);
        }

        handleIntent(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setMaxWidth(Integer.MAX_VALUE);

        int searchPlateId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_src_text", null, null);
        EditText searchPlate = searchView.findViewById(searchPlateId);
        searchPlate.setHint("Pesquisar");
        searchPlate.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search_copy, 0,0,0);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    class WrappedResponse {
        private List<Enterprise> enterprises;

        public List<Enterprise> getEnterprises() {
            return enterprises;
        }

        public void setEnterprises(List<Enterprise> enterprises) {
            this.enterprises = enterprises;
        }
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            new SearchTask(query).execute((Void) null);
        }
    }

    interface SearchService {
        @Headers({
                "Content-Type: application/json"
        })
        @GET("enterprises")

        Call<WrappedResponse> search(
                @Header("access-token") String accessToken,
                @Header("client") String client,
                @Header("uid") String uid,
                @Query("name") String query
        );
    }

    public class SearchTask extends AsyncTask<Void, Void, Void> {

        private final String query;

        SearchTask(String query) {
            this.query = query;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://54.94.179.135:8090/api/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            SearchService service = retrofit.create(SearchService.class);

            Call<WrappedResponse> login = service.search(
                    user.getAccessToken(),
                    user.getClient(),
                    user.getUid(),
                    query);

            results.clear();
            try {
                List<Enterprise> enterprises = login.execute().body().enterprises;
                System.out.println(enterprises);
                results.addAll(enterprises);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            recyclerView.setAdapter(mAdapter);
            if (!results.isEmpty()) {
                recyclerView.setVisibility(View.VISIBLE);
                searchMessage.setVisibility(View.GONE);
            } else {
                recyclerView.setVisibility(View.GONE);
                searchMessage.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            results.clear();
        }
    }

    class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

        private final RequestManager glide;
        private List<Enterprise> mDataset;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public CardView mCardView;
            public ViewHolder(CardView v) {
                super(v);
                mCardView = v;
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(List<Enterprise> myDataset, RequestManager glide) {
            mDataset = myDataset;
            this.glide = glide;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            CardView v = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.result_item, parent, false);
            // set the view's size, margins, paddings and layout parameters
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            final Enterprise enterprise = mDataset.get(position);
            ((TextView)holder.mCardView.findViewById(R.id.info_text)).setText(enterprise.getEnterprise_name());
            ((TextView)holder.mCardView.findViewById(R.id.type_text)).setText(enterprise.getEnterprise_type().getEnterprise_type_name());
            ((TextView)holder.mCardView.findViewById(R.id.location_text)).setText(enterprise.getCountry());

            ImageView imageView = holder.mCardView.findViewById(R.id.image_view_list);

            if (enterprise.getPhoto() != null) {
                glide
                        .load(enterprise.getPhoto())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(imageView);
            } else {
                glide
                        .load(R.drawable.img_e_1_lista)
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .into(imageView);
            }

            holder.mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
                    intent.putExtra("empresa", enterprise.getId());
                    intent.putExtra("user", user);
                    startActivity(intent);
                }
            });

        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }


}
